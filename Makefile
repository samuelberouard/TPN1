all: bin/VBO bin/texcubeIL bin/texcube

bin/texcube: src/texcube.c
	gcc -o bin/texcube src/texcube.c -lIL -std=c99 -lglut -lGLU -lGL -lm


bin/texcubeIL: src/texcube.c
	gcc -o bin/texcubeIL src/texcube.c -lIL -std=c99 -lglut -lGLU -lGL -lm -DIL_VERSION

bin/VBO: src/VBO.c include/point.h include/color.h
	gcc -o bin/VBO src/VBO.c -Iinclude -lglut -lGL -lGLU

clean:
	rm -rf bin/*
