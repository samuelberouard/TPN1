/**
 * Texturing
 */
#include <stdlib.h>

#include <GL/glut.h>

#ifdef IL_VERSION
#include <IL/il.h>
#endif

float angle = 0;


void displayFunc() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glLoadIdentity();

  glTranslatef(0, 0, -4);

  glRotatef(angle, 0, 1, 0.3);
  glRotatef(angle, 1, 0, 0.3);

  glBegin(GL_QUADS);

  glTexCoord2f(0.25f,  0.25f);
  glVertex3f(-1.0f, -1.0f,  1.0f);
  glTexCoord2f(0.25f,  0.00f);
  glVertex3f(-1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.50f, 0.00f);
  glVertex3f( 1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.50f, 0.25f);
  glVertex3f( 1.0f, -1.0f,  1.0f);

  glTexCoord2f(0.25f,  0.50f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glTexCoord2f(0.25f,  0.25f);
  glVertex3f(-1.0f, -1.0f,  1.0f);
  glTexCoord2f(0.50f,  0.25f);
  glVertex3f( 1.0f, -1.0f,  1.0f);
  glTexCoord2f(0.50f,  0.50f);
  glVertex3f( 1.0f, -1.0f, -1.0f);

  glTexCoord2f(0.00f,  0.50f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glTexCoord2f(0.25f,  0.50f);
  glVertex3f(-1.0f, -1.0f,  1.0f);
  glTexCoord2f(0.25f,  0.25f);
  glVertex3f(-1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.00f,  0.25f);
  glVertex3f(-1.0f,  1.0f, -1.0f);

  glTexCoord2f(0.75f,  0.50f);
  glVertex3f( 1.0f, -1.0f, -1.0f);
  glTexCoord2f(0.50f,  0.50f);
  glVertex3f( 1.0f, -1.0f,  1.0f);
  glTexCoord2f(0.50f,  0.25f);
  glVertex3f( 1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.75f,  0.25f);
  glVertex3f( 1.0f,  1.0f, -1.0f);

  glTexCoord2f(0.50f,  0.50f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glTexCoord2f(0.50f,  0.75f);
  glVertex3f(-1.0f,  1.0f, -1.0f);
  glTexCoord2f(0.25f,  0.75f);
  glVertex3f( 1.0f,  1.0f, -1.0f);
  glTexCoord2f(0.25f,  0.50f);
  glVertex3f( 1.0f, -1.0f, -1.0f);

  glTexCoord2f(0.50f,  1.00f);
  glVertex3f(-1.0f,  1.0f, -1.0f);
  glTexCoord2f(0.50f,  0.75f);
  glVertex3f(-1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.25f,  0.75f);
  glVertex3f( 1.0f,  1.0f,  1.0f);
  glTexCoord2f(0.25f,  1.00f);
  glVertex3f( 1.0f,  1.0f, -1.0f);

  glEnd();

  glutSwapBuffers();

}


void idleFunc()
{

  angle=(angle>360.0f) ? 0 : angle + 0.05f;
  glutPostRedisplay();
}


int main(int argc, char* argv[]) {

  glutInit(&argc, argv);
  glutInitWindowSize(600, 600);
  glutInitWindowPosition(100, 100);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

  glutCreateWindow("GLWindow");

  glutIdleFunc(idleFunc);
  glutDisplayFunc(displayFunc);

  glMatrixMode(GL_PROJECTION);

  glLoadIdentity();

  gluPerspective(50, 1, 0.001f, 50.0f);

  glMatrixMode(GL_MODELVIEW);

  //glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

  //  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  // activation des textures
  glEnable(GL_TEXTURE_2D);

  // définition de la texture
  unsigned int image;

#ifdef IL_VERSION

  ilInit();
  ilGenImages(1, &image);
  ilBindImage(image);
  ilLoadImage("in.png");
  unsigned int width, height, bpp, format;
  width = ilGetInteger(IL_IMAGE_WIDTH);
  height = ilGetInteger(IL_IMAGE_HEIGHT);
  bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
  format = ilGetInteger(IL_IMAGE_FORMAT);

  unsigned char* data = ilGetData();

#else

  unsigned int width  = 300;
  unsigned int height = 400;

  unsigned char * data = (unsigned char *)malloc( width * height * 4 );

  for( size_t i = 0 ; i < width * height ; ++i )
  {
    data[ 4 * i     ] = rand()%255;
    data[ 4 * i + 1 ] = rand()%255;
    data[ 4 * i + 2 ] = rand()%255;
    data[ 4 * i + 3 ] = 255;
  }
#endif


  // identifiant de texture non initialisé
  unsigned int texture;

  // génération d'un identifiant de texture
  glGenTextures(1, &texture);

  // activation de la texture, ici 2D, dans le contexte courant
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  // définition des caractéristiques de la texture et de son contenu.
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  glutMainLoop();

  return 0;

}
