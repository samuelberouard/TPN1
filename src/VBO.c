#include <stdio.h>

#include <GL/glut.h>

#include "../include/point.h"
#include "../include/color.h"

point_t points[] = {
  -0.5f, -0.5f, 0.0f,
  -0.5f, 0.5f, 0.0f,
  0.5f, 0.5f, 0.0f,
  0.5f, -0.5f, 0.0f
};

color_t colors[] = {
  1.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 1.0f,
  1.0f, 1.0f, 0.0f
};

unsigned char indices[] = {
  0, 1, 2,
  2, 3, 0
};

void translate(point_t* p, float dx, float dy, float dz){
  p->x += dx;
  p->y += dy;
  p->z += dz;
}

void invert(color_t* c){
  c->r = 1.0f-(c->r);
  c->g = 1.0f-(c->g);
  c->b = 1.0f-(c->b);
}

void init() {

  translate(&points[0], -0.2f, 0.55f, 0.4f);
  invert(&colors[0]);

  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_PROJECTION);

  glLoadIdentity();

  gluPerspective(100.0f, 1.0f, 0.01f, 200.0f);

  glMatrixMode(GL_MODELVIEW);
}


void initVBOs() {
  // Création des 3 identifiants des buffers
  unsigned int bufferids[3];
  glGenBuffers(3, bufferids);

  // Le buffer 0 contient les points
  glBindBuffer(GL_ARRAY_BUFFER, bufferids[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
  glVertexPointer(3, GL_FLOAT, 0, NULL); // Les points ne seront plus envoyés à la carte mais seront directement sur la carte.

  // Le buffer 1 contient les couleurs
  glBindBuffer(GL_ARRAY_BUFFER, bufferids[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
  glColorPointer(3, GL_FLOAT, 0, NULL);

  // Le buffer 2 contient les indices
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferids[2]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  // Activation des buffers
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_INDEX_ARRAY);
}


void display() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glLoadIdentity();

  glTranslatef(0.0f, 0.0f, -1.0f);

  // Affichage des éléments en fonction des buffers
  glDrawElements(GL_TRIANGLES, sizeof(indices), GL_UNSIGNED_BYTE, 0);

  glutSwapBuffers();

}

int main(int argc, char* argv[]) {

  glutInit(&argc, argv);

  glutInitWindowSize(600, 600);
  glutInitWindowPosition(100, 100);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

  glutCreateWindow("GLWindow");

  glutDisplayFunc(display);

  init();
  initVBOs();

  glutMainLoop();

  return 0;
}
